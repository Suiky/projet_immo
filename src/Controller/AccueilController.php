<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/accueil", name="accueil", methods={"GET"})
     */
    // public function accueil(ManagerRegistry $doctrine): Response
    // {
    //     $annonces = $doctrine->getRepository(Annonces::class)->findAll();

    //     return $this->render('accueil/index.html.twig', [
    //         'annonces' => $annonces
    //     ]);
    // }
}
